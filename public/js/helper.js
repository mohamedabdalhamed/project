function dropdowns(){	
	var dropdowns=document.querySelectorAll('.dropdown'); // Nodelist
	// console.log(dropdowns);
dropdowns.forEach(function(d){
var toggle=d.querySelector('.dropdown-toggle');
// console.log(toggle);
var menu=d.querySelector('.dropdown-menu');
// console.log(menu);

hideDropdownMenu(d);

toggle.addEventListener('click',function(e){
e.preventDefault();
toggleDropDownMenu(d);
});
document.addEventListener('click',function(e)
{
	if(!iselement(e.target,menu) && iselement(e.target,toggle)){
		hideDropdownMenu(d);

	}
});
if(d.classList.contains('a-dropdown')){

var menu_items=menu.querySelectorAll('.menu-item');
menu_items.forEach(function(menu_item){
menu_item.addEventListener('click',function(e){
e.preventDefault();
var text=e.target.dataset.text;
toggle.querySelector('.toggle-text').textContent=text;

})

});
}
});
}
function iselement(clickElme,element){
if(clickElme==element)
{
	return true;
}
if(clickElme.praenNode){
	return iselement(clickElme.praenNode,element);
}else{
	return false;
}
}
function toggleDropDownMenu(d){
	var menu=d.querySelector('.dropdown-menu');
if(menu.classList.contains('hide')){
	menu.classList.remove("hide");

}else{
	menu.classList.add("hide");

}
}
function hideDropdownMenu(d)
{
var menu=d.querySelector('.dropdown-menu');
menu.classList.add("hide");
}