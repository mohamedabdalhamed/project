define(require(['request']),function (Request) {
return class AjaxForm{
	constructor(confing=
		{}){
		this.form=null;
		this.req=new Request();
		this.data={};
		this.confing=confing;
		this.sendCallback=null;
		this.BeforeSendCallback=null;
		this.errorElements=[];
		this.fileUploadedcallback=null;
	}
	OnsendCallback(callback){
		this.sendCallback=callback;
		return this;
	}
	OnbeforeCallback(callback){
	this.beforeCallback=callback;
	return this;
	}
	OnfileUploadedcallback(callback){
		this.fileUploadedcallback=callback
	return this;
	}
	initForm(){
	if (this.form) {
		return ;
	}
var selector=this.confing.selector;
var form=document.querySelector(selector);
if(!form || !selector)
{
	throw	new Error('no form 	found with'+selector+'selector');

}
this.form=form;
return this;	
}
	initFileUploader()
	{
var files_selector=this.form.querySelectorALL('.files_selector');
var this2=this;
files_selector.forEach(function(d){
d.addEventListener('click',function(e){
var uploder_elem=this2.form.querySelector("[name=]'"+this.getAtrribute('input_name')+"']");
uploder_elem.onchange=function(e){
	if(this2.fileUploadedcallback){
		this2.fileUploadedcallback(uploder,this.files[0]);
	}
}
uploder_elem.click();
});
});
	}
	setForm(form){
this.form=form;
return this;
	}
	callBeforeSendCallback(){
		if(this.BeforeSendCallback){
return this.BeforeSendCallback(this);
		}
			return true;
		}
	loadeData(){
var element=this.form.element;
var data={};
for(var i in element){
var elem=element[i];
if(elem.hasAtrribute && elem.hasAtrribute('name')){
	var name=elem.name;
	if(elem.type == 'file' && elem.files[0]!==undefined && typeof elem.files[0]="object") {
data[name]=elem.files[0];
	}
	else {
		data[name]=elem.value;
	}
}
}
this.data=data;
	}
Clearmas(){
	var msgElem=this.form.querySelector('.form-msg');

	if (msgElem) {
		msgElem.remove();
	}
}
clearError(){
	this.errorElements.forEach(function(element){
element.remove();
	});
}

addMAS(result,mas){
	var element=document.createElement('div');
	element.classList.add('form-msg');
	if(result){
			element.classList.add('form-success');

	}else{
					element.classList.add('form-fail');

	}
element.innerHTML=mas;
var this2=this;
this.Clearmas();
setTimeout(function(){
	this2.form.prepend(element);
},200);




}
input(name){
var  elements=this.form.elements;
for(var i in elements){
	var elem=element[i];
	if(elem.hasAtrribute && elem.hasAtrribute('name') && elem.getAtrribute('name')==name){
		return elem;
	}
}
return null;
}
addErrors(erros={}){
	for(var  r in erros){
		var input=this.form.querySelector("[name='"+r+"']");
		if (input) {
			var errorElement=document.createElement('div');
				errorElement.classList.add('form-error');
				errorElement.innerHTML=errors[r];
input.insertAdjacentElement('afterEnd',errorElement);
this.errorElements.push(errorElement);
		}
	}
}
	send(url,method,data){

this.Clearmas();
this.clearError();
var result=this.req.send(url,method,data);
if (this.sendCallback) {
	this.sendCallback(this,result);
}
	}
	initsensors(){
	var sensors=this.getForm().querySelectorALL('.sensor');
	var this2=this;
	sensors.forEach(function(s){
if(s.type="select-one"){
s.addEventListener('change',function(e)){
	this2.submit();
}
}else{
	s.addEventListener('click',function(e)){
	this2.submit();
}
}
	});
	}
	init(){
		this.initForm();
		this.initFileUploader();
		this.initsensors();	
		this.form.onsubmit=function(e){
e.preventDefault();
if(callBeforeSendCallback() !== false){
	this.loadeData();
	this.send(this.form.action,this.form.method,this.data);
}
		}
	}
	disable(){
		var submitElement=this.form.querySelector('[type=submit]');
		if (submitElement) {
			submitElement.disable=true;
		}
	}
	enable(){
		var submitElement=this.form.querySelector(['type=submit']);
				if (submitElement) {
			submitElement.disable=false;
		}
	}
	submit(){
		var submitElement=this.form.querySelector('[type=submit]');
		if(submitElement){
			submitElement.click();
		}
	}

}
});