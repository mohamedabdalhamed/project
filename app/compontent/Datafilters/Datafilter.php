<?php 
namespace App\compontent\Datafilters;
use Illuminate\Database\Eloquent\builder;

abstract class Datafilter
{
	
	 protected $bulider;
	 protected $param=[];
	 protected $criteria;
	 protected $per_page;
	 protected $default_per_page=10;
	 protected $max_per_page=30;
	 protected $sortingMethods=[];

public function setBulider(builder $builder){
$this->builder=$builder;
return $this;

}
public function QueryBulider(){
	foreach (app('request')->Query() as  $key=>$value) {
		if(!empty($value)){
			if(method_exists($this, $key)){
			call_user_func_array([$this,$key], [$key,$value]);
			}elseif (!isset($this->param[$key])) {
					call_user_func_array([$this,'default_metdhod'], [$key,$value]);
	
			}
		}
	}
}
public function getData(){
 $this->per_page('per_page',$this->default_per_page);
 $paginator=$this->builder->paginate($this->per_page);
 $paginator->appends(app('request')->Query());
 	return  $paginator;
}
public function per_page($key,$value){

if (empty($this->$key)) {
      $this->per_page=$value<=$this->max_per_page?$value:$this->max_per_page;

// $this->$key=$value<=$this->max_per_page?$value:$this->max_per_page;
}
}
public function SetCirlce(array $criteria=[]){
foreach ($criteria as  $key=>$value) {
			if(method_exists($this, $key)){
			call_user_func_array([$this,$key], [$key,$value]);
			}elseif (!isset($this->param[$key])) {
					call_user_func_array([$this,'default_metdhod'], [$key,$value]);
	
			}
		
	}	
}
public function sort($key,$value)
{
if(!isset($this->$sortingMethods[$key])){
	$method=$this->sortingMethods[$key];
	call_user_func_array([$this,$method], []);
}
} 
 public abstract function default_metdhod($key,$value);

}
