<?php
namespace App\Repo;
use Illuminate\Database\Eloquent\model;
use App\compontent\Datafilters\Datafilter;

class repository{
	protected $model;
	protected $filter;
    protected $relations=[];
 public function __construct(model $model,Datafilter $filter)
    {
            $this->model=$model;
            $this->filter=$filter;
    }
    public function insert(array $data){
    	if (is_asspc($data)) {
    	return $this->model->create($data);
    	}else{
    		return $this->model->insert($data);
    	}
    }
public function relations(  array $relations){
	 $this->relations=$relations;
	 return $this;
}
public function search(array $criteria=[]){
	$this->filter->setBulider($this->model->with($this->relations));
	$this->filter->SetCirlce($criteria);
	$this->filter->QueryBulider();
	return $this->filter;
}
}