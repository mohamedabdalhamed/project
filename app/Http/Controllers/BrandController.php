<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Repo\BrandRepo;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['manage']);
    }
    public function manage(Request $req,BrandRepo $Brand)
    {
    	$this->Authorize('manage',Brand::class);
//     	foreach (range(1,100) as $i ) 
// {
// $Brand->insert(['name'=>'brand'.$i]);

// }
    	$df=$Brand->search();
$data=$df->getData();
$data_trage['data']=$data;
if($req->ajax()){
	$view=view('manage-list')->with($data)->render();
}
return view('Brand.manag',$data_trage);

    }
}
