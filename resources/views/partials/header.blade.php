<div class="header">
	<div class="header-top">
		<div class="row">
			<div  class="col col-lg-2 col-md-2 col-sm-12 col-xs-12">
				<a href="" class="logo">
					<h1 class="no-margin">mmazon</h1>
				</a>
			</div>
			<div  class="col col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<form action="" method="GET" class="msf">
					<div class="form-section section-left">
						<select name="" id="" class="form-control control-select">
							<option value="">All categories</option>
							<option value="">computer</option>
							<option value="">Laptop</option>

						</select>
						<a class="c-selector form-control"><span class="selector-face">All categories</span><i class="fa fa-chevron-down"></i></a>
					</div>
					<div class="form-section section-mid">
						<input type="text" name="Query" class="form-control control-text" placeholder="Search in Product....">
					</div>
					<div class="form-section section-right">
						<input type="submit" class="form-control control-submit" value="Search">
					</div>
				</form>
			</div>
			<div ></div>
		</div>
		<div class="header-mid">
			<div class="row">
				<div class="col col-offset-lg-2 col-offset-md-2 col-lg-6 col-md-6 col-xs-12 col-sm-12">
					<ul class="header-nav">
						<li class="relative"><a href="#" class="categories-toggle">All categories <i class="fa fa-chevron-down"></i></a>

<div class="a-list">
	<a href="" class="list-item">Electronics</a>
	<a href="" class="list-item">Desktop</a>
	<a href="" class="list-item">Laptop</a>
	<a href="" class="list-item">Watches</a>

</div>
						</li>

						<li><a href="#">Home</a></li>
						<li><a href="#">Track Order</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">About Us</a></li>
					</ul>
				</div>
				<div class="col col-lg-4 col-md-4  col-xs-12 col-sm-12">
				<ul class="header-nav multi-line">
					<li>
						<a href="#">
						<span class="header-nav-line header-nav-line-1">Welcome{{Auth::check()?auth()->user()->name:'sigin in'}}</span>	</a>
					
						<a href="#">	<span class="header-nav-line header-nav-line-2">Acount & lists <i class="fa fa-chevron-down"></i>	</span>

					</a>
					<div class="a-list">
	<a href="{{route('account')}}" class="list-item">Account</a>
	<a href="" class="list-item">Wish List</a>
@if(Auth::check())
    <a href="{{ route('logout') }}" class="list-item"
                                            onclick="event.preventDefault(); $this.querySelector('form.logout-form').submit();">

                                            Logout
<form action="{{route('logout')}}" method="POST" id="form.logout-form">
	@csrf
<input type="submit" class="hide">
</form>
                                        </a>
@else

<a href="{{route('login')}}" class="list-item">login</a>
<a href="{{route('register')}}" class="list-item">register</a>

@endif
</div>
		
				    </li>
				    <li>

				    	<a href="" class="header-nav-bold">
				    		Orders
				    	</a>
				    </li>
<li class="relative">
	<a href="" class="header-shop">
		<img src="{{asset('image/shopping.png')}}" >
				    	<span class="item">15</span>
		
	</a>

</li>
<li class="relative">
	<a href="" class="header-shop">
		<img src="{{asset('image/hearts.png')}}" >
				    	<span class="item">11</span>
		
	</a>

</li>
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>